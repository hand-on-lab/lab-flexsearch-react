const {
  Console
} = require("console");
const {
  Index,
  Document,
  Worker
} = require("flexsearch");
const fs = require('fs');
const docs = JSON.parse(fs.readFileSync('search-index.json', 'utf8'));

const indexType = {
  "engb": "engb",
  "zhcn": "zhcb"
};

let index = new Document({
  tokenize: "forward",
  optimize: true,
  resolution: 9,
  encode: str => str.replace(/[\x00-\x7F]/g, "").split(""),
  charset: "latin:simple",
  document: {
    id: "id",
    index: [
      "engbName",
      "engbNameAlias",
      "zhcnName",
      "zhcnNamePinyin",
      "zhcnNameAlias",
      "zhcnNameAliasPinyin"
    ]
  }
});

let searchInstance = {
  engbIndex: {},
  zhcnIndex: {},
  indexType: {
    "engb": "engb",
    "zhcn": "zhcn"
  },
  init: function (docs) {
    engbIndex=this.generateIndex(this.indexType.engb, docs);
    zhcnIndex=this.generateIndex(this.indexType.zhcn, docs);
  },
  searchResults: function (input, limit) {
    let results1 = engbIndex.search(input, limit);
    let results2 = zhcnIndex.search(input, limit);
    let result = results1.concat(results2);

    console.log(results1.length, results2.length, result.length)

    return result;
  },
  generateIndex: function (lang, docs) {
    let index = lang === this.indexType.zhcn ? new Document({
      tokenize: "forward",
      optimize: true,
      resolution: 9,
      encode: str => str.replace(/[\x00-\x7F]/g, "").split(""),
      document: {
        id: "id",
        index: [
          "engbName",
          "engbNameAlias",
          "zhcnName",
          "zhcnNamePinyin",
          "zhcnNameAlias",
          "zhcnNameAliasPinyin"
        ]
      }
    }) : new Document({
      tokenize: "forward",
      optimize: true,
      resolution: 9,
      charset: "latin:simple",
      document: {
        id: "id",
        index: [
          "engbName",
          "engbNameAlias",
          "zhcnName",
          "zhcnNamePinyin",
          "zhcnNameAlias",
          "zhcnNameAliasPinyin"
        ]
      }
    });

    docs.map((doc) => {
      index.add({
        id: doc.id,
        engbName: doc.engb.name,
        engbNameAlias: doc.engb.nameAlias,
        zhcnName: doc.zhcn.name,
        zhcnNamePinyin: doc.zhcn.namePinyin,
        zhcnNameAlias: doc.zhcn.nameAlias,
        zhcnNameAliasPinyin: doc.zhcn.nameAliasPinyin
      });
    });
    
    return index;
  }
}

// Tests!

searchInstance.init(docs);

console.log("Ja:", searchInstance.searchResults("Ja", 10));
console.log("J1:", searchInstance.searchResults("J1", 10));
console.log("1:", searchInstance.searchResults("1", 10));
console.log("l:", searchInstance.searchResults("l", 10));
console.log("本:", searchInstance.searchResults("本", 10));
console.log("甲:", searchInstance.searchResults("甲", 10));
console.log("联赛:", searchInstance.searchResults("联赛", 10));
console.log("HY:", searchInstance.searchResults("HY", 10));