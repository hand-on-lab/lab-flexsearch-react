import React, { useEffect, useMemo, useRef, useState } from "react";
// Hooks for client-side routing -
// not used here
// import { Link, useHistory } from 'react-router-dom'

// Hook for forming a drop-down list,
// meeting all accessibility criteria
import { useCombobox } from "downshift";

// Full text search by `name`
import FlexSearch from "flexsearch";

// Hook for making HTTP requests
import useSWR from "swr";

// Type and class for "inexact" search by `url`
import { Doc, FuzzySearch } from "./fuzzy-search";

// Hook to determine the current locale -
// not used here
// import { useLocale } from './hooks'

// Utilities for getting placeholder and
// set focus to input when pressing `/`,
// and also type for props to search
import { getPlaceholder, SearchProps, useFocusOnSlash } from "./search-utils";

const SHOW_INDEXING_AFTER_MS = 500;

// Types
// For data object
type Item = {
  id: number;
  rankNo: number;
  name: string;
  icon: string;
  nameAlias: string;
  nameAliasPinyin: string;
};

// For search index
type SearchIndex = {
  flex: any;
  fuzzy: FuzzySearch;
  items: null | Item[];
};

// For the search result object
type ResultItem = {
  id: number;
  rankNo: number;
  name: string;
  icon: string;
  nameAlias: string;
  nameAliasPinyin: string;
  positions: Set<number>;
};

// Hook for working with the search index
function useSearchIndex(): readonly [
  null | SearchIndex,
  null | Error,
  () => void
] {
  // Initialization state
  const [shouldInitialize, setShouldInitialize] = useState(false);
  // Index state
  const [searchIndex, setSearchIndex] = useState<null | SearchIndex>(null);

  let url = `/search-index.json`;

  // If initialization state is `true`,
  // call the `useSWR` hook:
  // the first argument is `url`, it is also the key for the cache,
  // the second one is the so-called `fetcher`, the function
  // for request-receiving data,
  // third - settings (in this case, we disable
  // re-request when focus is set).
  // This signature means that the hook is waiting to be initialized
  // https://swr.vercel.app/docs/conditional-fetching
  const { error, data } = useSWR<null | Item[], Error | undefined>(
    shouldInitialize ? url : null,
    async (url: string) => {
      // Get response
      const response = await fetch(url);
      if (!response.ok) {
        // If an error occurs, throw an exception
        // with message as text
        throw new Error(await response.text());
      }

      // Return the response in `JSON` format
      return await response.json();
    },
    // Disable revalidation
    { revalidateOnFocus: false }
  );

  // Doing a side effect
  useEffect(() => {
    // Do nothing if there is no data
    // or the presence of a search index
    if (!data || searchIndex) {
      return;
    }

    // Создаем поисковый индекс
    // `tokenize` - это режим индексации (в данном случае
    // используется инкрементальная индексация в прямом направлении, т.е. вперед)
    // https://github.com/nextapps-de/flexsearch#tokenizer-prefix-search
    const flex = new FlexSearch.Index({ 
      tokenize: "forward",
      // encode: str => str.replace(/[\x00-\x7F]/g, "").split("") 
    });

    // Данными является массив объектов с заголовками и адресами статей -
    // `{ title: 'Заголовок', url: 'https://...' }`
    // Перебираем объекты и помещаем заголовок и его индекс (в массиве) в поисковый индекс
    data!.forEach(({ name }, i) => {
      flex.add(i, name);
    });

    // Создаем экземпляр неточного поиска
    const fuzzy = new FuzzySearch(data as Doc[]);

    // Обновляем состояние поискового индекса
    setSearchIndex({ flex, fuzzy, items: data! });
  }, [searchIndex, shouldInitialize, data]);

  // Хук возвращает мемоизированный массив из 3 элементов:
  // поискового индекса, ошибки (которая может иметь значение `null`)
  // и функции для обновления состояния инициализации
  return useMemo(
    () => [searchIndex, error || null, () => setShouldInitialize(true)],
    [searchIndex, error, setShouldInitialize]
  );
}

// The fuzzy search is engaged if the search term starts with a '/'
// and does not have any spaces in it.
// Неточный поиск используется в случае, когда строка для поиска
// начинается с `/` и не содержит пробелов.
// Данная утилита определяет, должен ли использоваться такой поиск
function isFuzzySearchString(str: string) {
  return str.startsWith("/") && !/\s/.test(str);
}

// Утилита для подсветки совпадения
// при выполнении полнотекстового поиска
// `name` - это подходящий (совпавший) заголовок
// `q` - совпадение
function HighlightMatch({ name, q }: { name: string; q: string }) {
  // FlexSearch doesn't support finding out which "typo corrections"
  // were done unfortunately.
  // See https://github.com/nextapps-de/flexsearch/issues/99
  // `FlexSearch`, к сожалению, не умеет исправлять опечатки

  // Split on higlight term and include term into parts, ignore case.
  // Разделяем совпадение на части без учета регистра
  const words = q.trim().toLowerCase().split(/[ ,]+/);

  // $& means the whole matched string
  // `$&` means the entire string matched
  const regexWords = words.map((s) => s.replace(/[.*+?^${}()|[\]\\]/g, "\\$&"));

  const regex = `\\b(${regexWords.join("|")})`;
  // Разделяем заголовок на части по совпадению
  const parts = name.split(new RegExp(regex, "gi"));

  return (
    <b>
      {/* Перебираем части */}
      {parts.map((part, i) => {
        const key = `${part}:${i}`;
        // Если часть является совпадением
        if (words.includes(part.toLowerCase())) {
          // Подсвечиваем ее
          return <mark key={key}>{part}</mark>;
        } else {
          return <span key={key}>{part}</span>;
        }
      })}
    </b>
  );
  // Если ввести в поиске `query`, то данная утилита вернет такую разметку
  /*
    <b>
      <span>Document.</span>
      // совпадение
      <mark>query</mark>
      <span>Selector()</span>
    </b>
  */
}

// Тип для поискового виджета
type InnerSearchNavigateWidgetProps = SearchProps & {
  onResultPicked?: () => void;
  defaultSelection: [number, number];
};

// Хук для определения изменения значения инпута
function useHasNotChangedFor(value: string, ms: number) {
  // Состояние для изменений
  const [hasNotChanged, setHasNotChanged] = useState(false);
  // Предыдущее значение
  const previousValue = useRef(value);

  // Побочный эффект для определения наличия изменений
  useEffect(() => {
    // Если предыдущее значение равняется текущему,
    // ничего не делаем
    if (previousValue.current === value) {
      return;
    }
    // Обновляем предыдущее значение текущим
    previousValue.current = value;
    setHasNotChanged(false);
    // while timeouts are not accurate for counting time there error is only
    // upwards, meaning they might trigger after more time than specified,
    // which is fine in this case
    // Здесь речь идет о том, что таймеры не являются
    // хорошим средством для точного измерения времени,
    // поскольку могут срабатывать по истечении времени,
    // больше указанного, но в данном случае такой подход является приемлемым
    const timeout = setTimeout(() => {
      // Если значение инпута в течение указанного времени не изменилось,
      // значит, изменений не было :)
      setHasNotChanged(true);
    }, ms);
    // Очистка побочного эффекта во избежание утечек памяти
    return () => {
      clearTimeout(timeout);
    };
    // Хук запускается повторно при изменении значения инпута или задержки
  }, [value, ms]);

  // Возвращаем состояние
  return hasNotChanged;
}

// Виджет для поиска
function InnerSearchNavigateWidget(props: InnerSearchNavigateWidgetProps) {
  // Пропы:
  // значение инпута
  // функция для обработки изменения этого значения
  // индикатор того, находится ли инпут в фокусе
  // функция для обработки изменения состояния фокуса
  // функция для сохранения результатов поиска
  // выделение по умолчанию
  const {
    inputValue,
    onChangeInputValue,
    isFocused,
    onChangeIsFocused,
    onResultPicked,
    defaultSelection
  } = props;

  // Объект истории браузера и текущая локаль -
  // здесь не используются
  // const history = useHistory()
  // const locale = useLocale()

  // Получаем поисковый индекс, ошибку и функцию для запуска инициализации поискового индекса
  const [
    searchIndex,
    searchIndexError,
    initializeSearchIndex
  ] = useSearchIndex();

  // Ссылка на ипнут
  const inputRef = useRef<null | HTMLInputElement>(null);
  // Ссылка на форму
  const formRef = useRef<null | HTMLFormElement>(null);
  // Переменная для выделения
  const isSelectionInitialized = useRef(false);

  // Отображение результатов зависит от состояния изменений
  const showIndexing = useHasNotChangedFor(inputValue, SHOW_INDEXING_AFTER_MS);

  // Побочный эффект для инициализации выделения
  useEffect(() => {
    // Если отсутствует инпут или выделение не инициализировано, ничего не делаем
    if (!inputRef.current || isSelectionInitialized.current) {
      return;
    }

    // Если инпут находится в фокусе
    if (isFocused) {
      // Выделение по умолчанию имеет значение `[0, 0]`
      inputRef.current.selectionStart = defaultSelection[0];
      inputRef.current.selectionEnd = defaultSelection[1];
    }
    // Инициализируем выделение
    isSelectionInitialized.current = true;
  }, [isFocused, defaultSelection]);

  // Мемоизированные результаты поиска
  const resultItems: ResultItem[] = useMemo(() => {
    // Если отсутствует поисковый индекс или инпут, или при инициализации индекса возникла ошибка,
    // ничего не делаем
    if (!searchIndex || !inputValue || searchIndexError) {
      // This can happen if the initialized hasn't completed yet or
      // completed un-successfully.
      // Это может произойти, если инициализация еще не завершилась
      // или завершилась неудачно
      return [];
    }

    // The iPhone X series is 812px high.
    // If the window isn't very high, show fewer matches so that the
    // overlaying search results don't trigger a scroll.
    // Данное ограничение позволяет избежать появления
    // полосы прокрутки на экранах с маленькой высотой
    const limit = window.innerHeight < 850 ? 5 : 10;

    // Если строка начинается с `/` и не содержит пробелов,
    // выполняется неточный поиск
    if (isFuzzySearchString(inputValue)) {
      // Если значением инпута является `/`
      if (inputValue === "/") {
        // Возвращаем пустой массив
        return [];
      } else {
        // Выполняем поиск,
        // удаляя начальный `/`
        const fuzzyResults = searchIndex.fuzzy.search(inputValue.slice(1), {
          // Возвращается 5 или 10 лучших результатов
          limit
        });
        // Возвращаем массив объектов
        return fuzzyResults.map((fuzzyResult) => ({
          id: fuzzyResult.item.id,
          rankNo: fuzzyResult.item.rankNo,
          name: fuzzyResult.item.name,
          icon: fuzzyResult.item.icon,
          nameAlias: fuzzyResult.item.nameAlias,
          nameAliasPinyin: fuzzyResult.item.nameAliasPinyin,
          positions: fuzzyResult.positions
        }));
      }
    } else {
      // Full-Text search
      // Выполняем полнотекстовый поиск
      const indexResults: number[] = searchIndex.flex.search(inputValue, {
        // Ограничение
        limit,
        // Предположения
        suggest: true // This can give terrible result suggestions
        // Это может приводить к ужасным результатам :)
      });
      // Возвращаем массив оригинальных объектов
      return indexResults.map(
        (index: number) => (searchIndex.items || [])[index] as ResultItem
      );
    }
    // Результаты вычисляются повторно при изменении
    // значения инпута, поискового индекса или возникновении ошибки
  }, [inputValue, searchIndex, searchIndexError]);

  // Базовый адрес для выполнения HTTP-запроса
  // const formAction = `/${locale}/search`
  const formAction = `https://developer.mozilla.org/ru-RU/search`;
  // Формируем полный путь для поиска
  const searchPath = useMemo(() => {
    // Новая строка запроса
    const sp = new URLSearchParams();
    // Получается `?q=inputValue`
    sp.set("q", inputValue.trim());
    // Возвращаем полный путь
    // Если ввести в инпуте `qwerty`,
    // вернется `https://developer.mozilla.org/ru/search?q=qwerty`
    return `${formAction}?${sp.toString()}`;
  }, [formAction, inputValue]);

  // Пустой результат - когда ничего не найдено
  const nothingFoundItem = useMemo(
    () => ({ id:0,rankNo:0,name:"",icon:"",nameAlias:"",nameAliasPinyin:"", positions: new Set() }),
    [searchPath]
  );

  // Формируем выпадающий список, отвечающий всем критериям доступности
  const {
    getInputProps,
    getItemProps,
    getMenuProps,
    getComboboxProps,

    // Индекс элемента, на которого наведен курсор или который находится в фокусе
    highlightedIndex,
    // Индикатор того, находится ли меню в открытом состоянии
    isOpen,

    reset,
    toggleMenu
  } = useCombobox({
    // Результаты поиска или пустой результат в виде массива из одного элемента
    items: resultItems.length === 0 ? [nothingFoundItem] : resultItems,
    // Значение инпута
    inputValue,
    // Список по умолчанию будет находится в открытом состоянии
    // только при фокусировке на инпуте
    defaultIsOpen: isFocused,
    // При клике по выделенному (highlightedIndex) результату
    onSelectedItemChange: ({ selectedItem }) => {
      if (selectedItem) {
        // history.push(selectedItem.url)
        // Открывается новое окно с соответствующей статьей на MDN
        
        // window.open(selectedItem.url, "_blank");
        
        // Значение инпута обнуляется
        onChangeInputValue("");
        // Выполняется сброс меню
        reset();
        // Меню скрывается
        toggleMenu();
        // Расфокусировка
        inputRef.current?.blur();
        // Если передана функция для сохранения результатов,
        // вызываем ее
        if (onResultPicked) {
          onResultPicked();
        }
        // Плавная прокрутка в верхнюю часть области просмотра
        window.scroll({
          top: 0,
          left: 0,
          behavior: "smooth"
        });
      }
    }
  });

  // При нажатии `/` на инпут устанавливается фокус
  useFocusOnSlash(inputRef);

  // Побочный эффект для инициализации поискового индекса
  useEffect(() => {
    // Если инпут находится в фокусе
    if (isFocused) {
      // Выполняем инициализацию
      initializeSearchIndex();
    }
  }, [initializeSearchIndex, isFocused]);

  // Результаты поиска - рендеринг
  // значением переменной является `IIFE` - автоматически вызываемое функциональное выражение
  const searchResults = (() => {
    // Если меню закрыто, т.е. инпут не находится в фокусе,
    // или значение инпута является пустая строка (без учета пробелов),
    // ничего не делаем
    if (!isOpen || !inputValue.trim()) {
      return null;
    }

    // Если при инициализации поискового индекса возникла ошибка
    if (searchIndexError) {
      return (
        // <div className='searchindex-error'>Error initializing search index</div>
        <div className="searchindex-error">
          При инициализации поискового индекса возникла ошибка
        </div>
      );
    }

    // Если инициализация индекса еще не завершилась
    if (!searchIndex) {
      // и не было изменений значения инпута на протяжении 500 мс
      return showIndexing ? (
        <div className="indexing-warning">
          {/* <em>Initializing index</em> */}
          <em>Инициализация индекса</em>
        </div>
      ) : null;
    }

    return (
      <>
        {/* Если отсутствуют результаты поиска и значением инпута не является `/` */}
        {resultItems.length === 0 && inputValue !== "/" ? (
          <div
            {...getItemProps({
              className:
                "nothing-found result-item " +
                (highlightedIndex === 0 ? "highlight" : ""),
              // Возвращаем пустой результат
              item: nothingFoundItem,
              index: 0
            })}
          >
            {/* No document titles found. */}
            No matching data found.
            <br />
          </div>
        ) : (
          // Перебираем результаты поиска
          resultItems.map((item, i) => (
            <div
              {...getItemProps({
                key: item.name,
                className:
                  "result-item " + (i === highlightedIndex ? "highlight" : ""),
                item,
                index: i
              })}
            >
              {/* Выполняем подсветку совпадения */}
              <HighlightMatch name={item.name} q={inputValue} />
              <br />
              {/* Формируем хлебные крошки для адреса статьи */}
              {/*<BreadcrumbURI uri={item.url} positions={item.positions} />*/}
            </div>
          ))
        )}
        {/* Если выполняется неточный поиск */}
        {isFuzzySearchString(inputValue) && (
          // <div className='fuzzy-engaged'>Fuzzy searching by URI</div>
          <div className="fuzzy-engaged">Fuzzy searching by Pinyi</div>
        )}
      </>
    );
  })();

  // Рендеринг
  return (
    <form
      action={formAction}
      {...getComboboxProps({
        // Ссылка на форму
        ref: formRef as any, // downshift's types hardcode it as a div
        // для `downshift` все есть `div` с точки зрения типов :)
        className: "search-form",
        id: "nav-main-search",
        role: "search",
        // Обработчик отправки формы
        onSubmit: (e) => {
          // This comes into effect if the input is completely empty and the
          // user hits Enter, which triggers the native form submission.
          // When something *is* entered, the onKeyDown event is triggered
          // on the <input> and within that handler you can
          // access `event.key === 'Enter'` as a signal to submit the form.

          // Здесь речь идет о том, что если инпут является пустым и
          // пользователь нажимает `Enter`, выполняется нативная отправка формы.
          // Если же инпут не является пустым, отправку формы можно перехватить
          // с помощью`event.key === 'Enter'` в обработчике
          // события нажатия клавиши `onKeyDown`.
          // Отключаем дефолтную обработку отправки формы
          e.preventDefault();
          // Это сломало кнопку для отправки формы в виде лупы (см. ниже)
        }
      })}
    >
      <div className="search-field">
        <label htmlFor="main-q" className="visually-hidden">
          {/* Search MDN */}
          Поиск MDN
        </label>

        <input
          {...getInputProps({
            type: "search",
            className: isOpen
              ? "has-search-results search-input-field"
              : "search-input-field",
            id: "main-q",
            name: "q",
            // Формируем плейсхолдер с помощью утилиты
            placeholder: getPlaceholder(isFocused),
            // При наведении курсора на инпут
            // запускается инициализация поискового индекса
            onMouseOver: initializeSearchIndex,
            // Метод для обновления состояния фокуса
            onFocus: () => {
              onChangeIsFocused(true);
            },
            // и здесь
            onBlur: () => onChangeIsFocused(false),
            // Обработчик нажатия клавиш
            onKeyDown(event) {
              // Если нажата клавиша `Escape` и имеется инпут
              if (event.key === "Escape" && inputRef.current) {
                // Меню скрывается, инпут очищается
                toggleMenu();
              } else if (
                // Если нажата клавиша `Enter`, инпут не является пустым и отсутствует выделенный результат поиска
                event.key === "Enter" &&
                inputValue.trim() &&
                highlightedIndex === -1
              ) {
                // Убираем фокус с инпута
                inputRef.current!.blur();
                // Отправляем форму
                // formRef.current!.submit()
                // Переходим на страницу поиска MDN
                window.open(searchPath, "_blank");
              }
            },
            // обработчик изменения значения инпута
            onChange(event) {
              if (event.target instanceof HTMLInputElement) {
                onChangeInputValue(event.target.value);
              }
            },
            // Ссылка на инпут
            ref: (input) => {
              inputRef.current = input;
            }
          })}
        />

        {/* Кнопка для отправки формы, которая не работала */}
        {/* создал пулреквест :) */}
        <input
          type="submit"
          className="search-button"
          value=""
          aria-label="Поиск"
          onClick={() => {
            if (inputValue.trim() && inputValue !== "/") {
              // history.push(searchPath)
              window.open(searchPath, "_blank");
            }
          }}
        />
      </div>

      {/* Выпадающий список с результатами */}
      <div {...getMenuProps()}>
        {searchResults && <div className="search-results">{searchResults}</div>}
      </div>
    </form>
  );
}

// Предохранитель - обработчик ошибок
class SearchErrorBoundary extends React.Component {
  // Состояние ошибки
  state = { hasError: false };

  // Регистрация ошибки
  static getDerivedStateFromError(error: Error) {
    // console.error('There was an error while trying to render search', error)
    console.error("В процессе рендеринга поиска возникла ошибка", error);
    // Обновляем состояние ошибки
    return { hasError: true };
  }
  render() {
    return this.state.hasError ? (
      // <div>Error while rendering search. Check console for details.</div>
      <div>
        An error occurred while rendering the search. Check the console for
        details.
      </div>
    ) : (
      this.props.children
    );
  }
}

// Экспорт виджета для поиска, обернутого в предохранитель
export default function SearchNavigateWidget(
  props: InnerSearchNavigateWidgetProps
) {
  return (
    <SearchErrorBoundary>
      <InnerSearchNavigateWidget {...props} />
    </SearchErrorBoundary>
  );
}
