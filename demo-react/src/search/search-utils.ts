import React, { useEffect } from "react";

// Type for search props
export type SearchProps = {
  inputValue: string;
  onChangeInputValue: (value: string) => void;
  isFocused: boolean;
  onChangeIsFocused: (isFocused: boolean) => void;
};

// Hook to set focus to input when pressing `/`
// Takes a reference to an input
export function useFocusOnSlash(
  inputRef: React.RefObject<null | HTMLInputElement>
) {
  useEffect(() => {
    // Perhaps the user wants to set focus to an input :)
    function focusOnSearchMaybe(event: KeyboardEvent) {
      const element = event.target as HTMLInputElement | HTMLTextAreaElement;
      // Link to input
      const input = inputRef.current;
      // If `/` is pressed and the name of the event target tag is not the specified tags
      if (
        event.code === "Slash" &&
        !["TEXTAREA", "INPUT"].includes(element.tagName)
      ) {
        // If there is an input and it is not in focus
        if (input && document.activeElement !== input) {
          event.preventDefault();
          input.focus();
        }
      }
    }
    // Register the handler on the document
    document.addEventListener("keydown", focusOnSearchMaybe);
    // Remove the handler when the component is unmounted
    return () => {
      document.removeEventListener("keydown", focusOnSearchMaybe);
    };
  }, [inputRef]);
}

// Utility to determine mobile phone
function isMobileUserAgent() {
  return (
    typeof window !== "undefined" &&
    (typeof window.orientation !== "undefined" ||
      navigator.userAgent.indexOf("IEMobile") !== -1)
  );
}

const ACTIVE_PLACEHOLDER = "Go ahead. Type your search...";

// Make this one depend on figuring out if you're on a mobile device
// because there you can't really benefit from keyboard shortcuts.
// Пассивный плейсхолдер - зависит от девайса
const INACTIVE_PLACEHOLDER = isMobileUserAgent()
  ? "Site search..."
  : 'Site search... (Press "/" to focus)';

// Utility to get placeholder
export const getPlaceholder = (isFocused: boolean) =>
  isFocused ? ACTIVE_PLACEHOLDER : INACTIVE_PLACEHOLDER;
